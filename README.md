# thanos-build

This project can build a thanos binary for an arbitrary ref.

https://gitlab.com/gitlab-com/gl-infra/thanos-build/-/pipelines/new

- `RELEASE_TARGET_REF`: `e414597420da83441c2a7f7fa39dc45c396faee2`

The binary as published to the package registry:

https://gitlab.com/gitlab-com/gl-infra/thanos-build/-/packages
