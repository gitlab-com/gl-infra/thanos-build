#!/bin/bash

set -efo pipefail

echo "Cloning..."
git clone https://github.com/thanos-io/thanos
cd thanos

if [[ -n $RELEASE_TARGET_REF ]]; then
    git checkout $RELEASE_TARGET_REF
fi

export VERSION="$(git rev-parse --short HEAD)"
export RELEASE="thanos-$VERSION.linux-amd64"

if curl -I --silent --fail --header "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/thanos/$VERSION/$RELEASE.tar.gz" 2>/dev/null; then
    echo "package for version $VERSION already exists"
    echo "see: https://gitlab.com/${CI_PROJECT_PATH}/-/packages"
    exit 0
fi

echo "Building..."
GOBIN=$(pwd)/.build/scratch
mkdir -p "$GOBIN"
make GOBIN=$GOBIN build

mkdir -p .build/$RELEASE
mv $GOBIN/thanos .build/$RELEASE
cd .build

echo "Packaging..."
tar czfv $RELEASE.tar.gz $RELEASE

if [[ -n $RELEASE_TARGET_REF ]]; then
    echo "Uploading..."
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $RELEASE.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/thanos/$VERSION/$RELEASE.tar.gz"
fi
